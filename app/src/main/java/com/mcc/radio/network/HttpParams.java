package com.mcc.radio.network;

public class HttpParams {

    public static final String BASE_URL = "https://script.google.com/macros/s/AKfycbygukdW3tt8sCPcFDlkMnMuNu9bH5fpt7bKV50p2bM/";
    public static final String SHEET_API_END_POINT = "exec?"; // sheet api end point

    // Sheet Information
    public static final String SHEET_ID = "1EXWQfsLuPwNCfHNh01b8a7ylxISYp6VW4PYXEWdph5I"; // Replace by your sheet id
    public static final String PROGRAM_SHEET_NAME = "programs"; // Replace by your sheet name
    public static final String RADIO_URL_SHEET_NAME = "radio_channel"; // Replace by your sheet name

}
