package com.mcc.radio.listeners;
import android.widget.ImageView;

public interface ListItemClickListener {
    void onAlarmIconClick(ImageView view, int position);
}
