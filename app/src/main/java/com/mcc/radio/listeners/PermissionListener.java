package com.mcc.radio.listeners;

public interface PermissionListener {
    void onPermissionGranted();
}
